<?php

namespace Tests\Feature;

use App\Models\Area;
use App\Models\Disciplina;
use App\Models\Laboratorio;
use App\Models\RegistroLaboratorio;
use App\Models\Rol;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegistroLaboratorioControllerTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba

    public function testGetLaboratorioByIdLaboratorioPublic(){

        $rol = Rol::factory()->create(['nombre' => 'Rol de prueba', 'estado' => true]);
        $user = User::factory()->create();
        $laboratorio = Laboratorio::factory()->create(['nombre' => 'Laboratorio 1', 'estado' => true]);
        $area = Area::factory()->create(['nombre' => 'Área de prueba', 'estado' => true]);
        $disciplina = Disciplina::factory()->create(['nombre' => 'Disciplina de prueba', 'estado' => true]);
        // Crear un registro de laboratorio utilizando el Factory
        $registroLaboratorio = RegistroLaboratorio::factory()->create([
            'coordinador_id' => $user->usuario_id, // Reemplaza con valores apropiados
            'laboratorio_id' => $laboratorio->laboratorio_id,
            'area_id' => $area->area_id,
            'disciplina_id' => $disciplina->disciplina_id,
            'ubicacion' => "ubicacion prueba",
            'mision' => "mision prueba",
            'vision' => "vision prueba",
            'historia' => "historia prueba",
            'estado' => true,
        ]);
        
        $response = $this->get("/api/laboratorioCoordinador/$registroLaboratorio->registro_id");

        $response->assertStatus(200);

        $response->assertStatus(200)
            ->assertJsonStructure(['laboratorio']);

    }
}
