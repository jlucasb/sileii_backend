<?php

namespace Tests\Feature;

use App\Models\Equipo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Solicitud;
use App\Models\User;


class SolicitudTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
      
        $response = $this->get('/solicitudes');

        $response->assertStatus(200)
            ->assertJsonStructure(['solicitudes']);
    }

    public function testStore()
    {
        // Simula una solicitud POST a '/solicitudes'
        $response = $this->json('POST', '/solicitudes', [
            'usuario_id' => 1,
            'equipo_id' => 1,
            'fecha_solicitud' => '2023-11-06',
            'detalle' => 'Detalle de la solicitud',
            'oficio' => 'Número de oficio',
        ]);

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'status'
        $response->assertJson(['status' => 'Creado correctamente']);
    }

    public function testShow()
    {
        // Crea una solicitud ficticia en la base de datos
        $solicitud = Solicitud::factory()->create();

        // Simula una solicitud GET a '/solicitud/{idsolicitud}'
        $response = $this->get("/solicitud/{$solicitud->solicitud_id}");

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'solicitud'
        $response->assertJsonStructure(['solicitud']);
    }
    public function testUpdate()
    {
        // Crea una solicitud ficticia en la base de datos
        $solicitud = Solicitud::factory()->create();

        // Simula una solicitud PUT a '/solicitud/{idsolicitud}'
        $response = $this->json('PUT', "/solicitud/{$solicitud->solicitud_id}", [
            'etapa' => 'Nueva Etapa',
        ]);

        // Verifica que la solicitud fue exitosa (código de estado HTTP 200)
        $response->assertStatus(200);

        // Verifica que la respuesta contiene un JSON con la clave 'status'
        $response->assertJson(['status' => 'Actualizado correctamente']);
    }
}
