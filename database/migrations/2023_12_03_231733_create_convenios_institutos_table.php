<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Se crea el migrate de convenios para instituto
        Schema::create('convenios_instituto', function (Blueprint $table) {

            $table->id('convenio_id');
            $table->unsignedInteger('instituto_id');
            $table->string('entidad');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->text('objetivo');
            $table->text('detalles');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //se aplica la validaciion de foreing 
        Schema::table('convenios_instituto', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
        });
        Schema::dropIfExists('convenios_instituto');
    }
};
