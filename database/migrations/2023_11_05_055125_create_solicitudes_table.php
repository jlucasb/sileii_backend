<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
          $table->id('solicitud_id');
          $table->unsignedInteger('usuario_id');
          $table->unsignedInteger('laboratorio_id');
          $table->unsignedInteger('equipo_id');
          $table->date('fecha_solicitud');
          $table->text('detalle');
          $table->text('oficio');
          $table->string('etapa',255);
          $table->boolean('estado')->default(true);
          $table->timestamps();


          $table->foreign('usuario_id')->references('usuario_id')->on('users')->onUpdate('cascade')->onDelete('cascade');

          $table->foreign('equipo_id')->references('equipo_id')->on('equipos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['equipo_id']);
        });
        Schema::dropIfExists('solicitudes');
    }
};
