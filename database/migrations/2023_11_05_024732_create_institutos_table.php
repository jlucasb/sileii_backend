<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutos', function (Blueprint $table) {
            $table->increments('instituto_id');
            $table->string('nombre', 255);
            $table->text('mision')->nullable();
            $table->text('vision')->nullable();
            $table->text('historia')->nullable();
            $table->text('ubicacion')->nullable();
            $table->unsignedInteger('usuario_director');
            $table->text('comite_directivo')->nullable();
            $table->text('contacto')->nullable();
            $table->string('nombre_imagen')->nullable();
            $table->string('imagen_instituto')->nullable();
            $table->text('url_instituto')->nullable();
            $table->text('url_facebook')->nullable();
            $table->boolean('estado')->default(true);
            $table->timestamps();
            $table->foreign('usuario_director')->references('usuario_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institutos', function (Blueprint $table) {
            $table->dropForeign(['usuario_director']);
        });
        Schema::dropIfExists('institutos');
    }
};
