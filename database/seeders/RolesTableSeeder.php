<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Seeder para llenar la tabla Roles en la base de datos con los campos correspondientes a cada Rol
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
        [
            'rol_id' => 1,
            'nombre' => 'Administrador',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 2,
            'nombre' => 'Coordinador',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 3,
            'nombre' => 'Operador',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 4,
            'nombre' => 'Director',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 5,
            'nombre' => 'Comite directivo',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 6,
            'nombre' => 'AdminLaboratorios',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],
        [
            'rol_id' => 7,
            'nombre' => 'AdminInstitutos',
            'estado' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]
      ]);
    }
}
