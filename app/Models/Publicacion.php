<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    use HasFactory;

    // Table
    protected $table = 'publicaciones';

    // Llave primaria
    protected $primaryKey = 'publicacion_id';

    protected $fillable = [
        'registro_id',
        'titulo',
        'link',
        'estado'
      ];

    public function laboratorio(){
        return $this->hasOne(RegistroLaboratorio::class, 'registro_id', 'registro_id');
    }
}
