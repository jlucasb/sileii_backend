<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    use HasFactory;

    protected $table = 'solicitudes'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'solicitud_id'; // Clave primaria personalizada

    protected $fillable = [
        'solicitud_id',
        'usuario_id',
        'laboratorio_id',
        'equipo_id',
        'fecha_solicitud',
        'detalle',
        'oficio',
        'etapa',
        'estado',
    ];


    public function equipo(){
        return $this->hasOne(Equipo::class, 'equipo_id', 'equipo_id');
    }

    public function usuario(){
        return $this->hasOne(User::class, 'usuario_id', 'usuario_id');
    }

    public function registro_laboratorio(){
        return $this->hasOne(RegistroLaboratorio::class, 'registro_id', 'laboratorio_id');
    }
}
