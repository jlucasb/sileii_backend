<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;

    protected $table = 'proyectos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'proyecto_id'; // Clave primaria personalizada

    protected $fillable = [
        'registro_id',
        'nombre_proyecto',
        'investigador_principal',
        'coinvestigadores',
        'nombre_imagen',
        'imagen_referencial',
        // 'imagen_desarrollo_proyecto',
        'doi',
        'resumen',
        'iba',
        'estado',
        // 'imagen_desarrollo_proyecto_url',
    ];

    protected $casts = [
      'coinvestigadores' => 'array',
    ];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->imagen_referencial);
    }

}
