<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuncionesInstituto extends Model
{
    use HasFactory;

    protected $table = 'funciones_instituto'; // Nombre real de tu tabla en la base de datos
    protected $primaryKey = 'funcion_id'; // Nombre de la clave primaria
    protected $fillable = ['instituto_id', 'funcion', 'estado']; // Campos que se pueden llenar masivamente

    // Relación con el modelo Instituto
    public function instituto()
    {
        return $this->belongsTo(Instituto::class, 'instituto_id', 'instituto_id');
    }  
}
