<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvenioInstituto extends Model
{
    use HasFactory;

    protected $table = 'convenios_instituto';

    protected $primaryKey = 'convenio_id';

    protected $fillable = [
        'instituto_id',
        'entidad',
        'fecha_inicio',
        'fecha_fin',
        'objetivo',
        'detalles',
        'estado',
    ];

    // protected $casts = [
    //     'estado' => 'boolean',
    //     'fecha_inicio' => 'date',
    //     'fecha_fin' => 'date',
    // ];

    public function instituto()
    {
        return $this->belongsTo(Instituto::class, 'instituto_id', 'instituto_id');
    }  
    
}
