<?php

namespace App\Http\Middleware;

use App\Models\Rol;
use Closure;
use Illuminate\Http\Request;

class AdminLaboratoriosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        $rol = Rol::where('rol_id','=',$user->rol_id)->first();

        if (strtolower($rol->nombre) !== 'adminlaboratorios' ) {
            return response()->json(['message' => 'El usuario no tiene permisos para realizar esta acción'], 401);
        }

        return $next($request);
    }
}
