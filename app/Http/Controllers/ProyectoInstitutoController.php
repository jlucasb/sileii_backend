<?php

namespace App\Http\Controllers;

use App\Models\ProyectoInstituto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProyectoInstitutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idinstituto)
    {
        // Mostramos los proyectos que estan relacionados con el registro de laboratorio
        $proyectos = ProyectoInstituto::where('instituto_id', $idinstituto)->orderBy('proyecto_id', 'asc')->get();

        return response()->json(['proyectos' => $proyectos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $proyecto = new ProyectoInstituto;
            $proyecto->instituto_id = $request->instituto_id;
            $proyecto->nombre_imagen = $request->nombre_imagen;
            $proyecto->nombre_proyecto = $request->nombre_proyecto;
            $proyecto->investigador_principal = $request->investigador_principal;
            $proyecto->coinvestigadores = $request->coinvestigadores;
            $proyecto->etapa = "INICIO";
            $proyecto->fecha_inicio = $request->fecha_inicio;
            $proyecto->fecha_finalizacion = $request->fecha_finalizacion;
            $proyecto->descripcion = $request->descripcion;

            // Si se envia una imagen
            if ($request->hasFile('imagen_referencial')) {
              $path = $request->file('imagen_referencial')->store('public/imagenes/proyectosInstituto');
              $proyecto->imagen_referencial = substr($path, 7);

                // $image = $request->file('imagen_referencial');
                // $imageName = time() . '.' . $image->getClientOriginalExtension();
                // $image->storeAs('images', $imageName, 'public'); // Guarda la imagen en la carpeta 'public/images'
                
                // // Registra la ruta en la base de datos
                // $proyecto->imagen_referencial = 'images/' . $imageName; // Ruta relativa a la carpeta de almacenamiento
            }            
            $proyecto->save();

            return response()->json(['status' => 'Creado Correctamente']); 
        } catch (QueryException $e) {
          return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProyectoInstituto  $proyectoInstituto
     * @return \Illuminate\Http\Response
     */
    public function show($idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $proyecto = ProyectoInstituto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto){
            return response()->json(['proyecto' => $proyecto]);
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProyectoInstituto  $proyectoInstituto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        //Validamos que el id sea el mismo y cogemos el primero
        $proyecto = ProyectoInstituto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto){
            try{
                $proyecto->nombre_imagen = $request->nombre_imagen;
                $proyecto->nombre_proyecto = $request->nombre_proyecto;
                $proyecto->investigador_principal = $request->investigador_principal;
                $proyecto->coinvestigadores = $request->coinvestigadores;
                $proyecto->etapa = $request->etapa;
                $proyecto->descripcion = $request->descripcion;
                $proyecto->fecha_inicio = $request->fecha_inicio;
                $proyecto->fecha_finalizacion = $request->fecha_finalizacion;
                

                if($request->hasFile('imagen_referencial')) {
                  Storage::delete($proyecto->imagen_referencial); // Eliminamos el archivo antiguo
                  $path = $request->file('imagen_referencial')->store('public/imagenes/proyectosInstituto'); // Guardamos el nuevo archivom
                  $proyecto->imagen_referencial = substr($path, 7);
                }

                $proyecto->update();
                
                return response()->json(['status' => 'Actualizado Correctamente']); 
            } catch (QueryException $e){
                return response()->json(['message'=> 'Ocurrio un error', 'error'=> $e]);
            }
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProyectoInstituto  $proyectoInstituto
     * @return \Illuminate\Http\Response
     */
    public function destroy($idproyecto)
    {
        $idproyecto = (int) $idproyecto;
        //Verificar que el idproyecto es de tipo integer
        if($idproyecto === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $proyecto = ProyectoInstituto::where('proyecto_id','=',$idproyecto)->first();
        if($proyecto && $proyecto->estado){
            $proyecto->estado = false; //Cambiamos el estado a false
            $proyecto->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Proyecto no encontrada'], 404);
        }
    }
}
